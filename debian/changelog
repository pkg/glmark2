glmark2 (2023.01+dfsg-1+apertis1) apertis; urgency=medium

  * Refresh the automatically detected licensing information

 -- Apertis CI <devel@lists.apertis.org>  Tue, 11 Mar 2025 13:28:55 +0000

glmark2 (2023.01+dfsg-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Apr 2023 12:49:24 +0000

glmark2 (2023.01+dfsg-1) unstable; urgency=medium

  * Team upload.

  [ Dylan Aïssi ]
  * New upstream release.
  * Also track upstream tags with debian/watch
  * Refresh patch
  * Update standards version to 4.6.2, no changes needed

  [ Debian Janitor ]
  * Apply multi-arch hints. + glmark2-data: Add Multi-Arch: foreign.

 -- Dylan Aïssi <daissi@debian.org>  Wed, 08 Feb 2023 10:22:15 +0100

glmark2 (2022.06~git.3f1104d-1+apertis0) apertis; urgency=medium

  * Sync updates from Debian Unstable.

 -- Dylan Aïssi <dylan.aissi@collabora.com>  Tue, 28 Jun 2022 13:56:54 +0200

glmark2 (2022.06~git.3f1104d-1) unstable; urgency=medium

  * Team upload.
  * New upstream snapshot.
  * Update standards version to 4.6.1, no changes needed.
  * Rules-Requires-Root: no

 -- Dylan Aïssi <daissi@debian.org>  Fri, 24 Jun 2022 13:13:14 +0200

glmark2 (2021.10~git.e359bb0-2) unstable; urgency=medium

  * Add Provides/Replaces to ease transition from Ubuntu packages.
  * Prefer pkgconf to pkg-config.
  * Bump debhelper from old 12 to 13.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.6.0, no changes needed.

 -- Andrej Shadura <andrew.shadura@collabora.co.uk>  Thu, 18 Nov 2021 11:13:03 +0100

glmark2 (2021.10~git.e359bb0-1) unstable; urgency=medium

  * New upstream snapshot.
  * Update the watch file.

 -- Andrej Shadura <andrew.shadura@collabora.co.uk>  Thu, 18 Nov 2021 10:51:58 +0100

glmark2 (2021.02+ds-1) unstable; urgency=low

  * Initial release. (Closes: #695849)
    Includes 2 patches from upstream to fix two issues, one related to wayland
    and one about creating two EGLDisplays which might cause issues on some
    platforms.

 -- Marius Vlad <marius.vlad@collabora.com>  Fri, 07 May 2021 13:29:55 +0200

glmark2 (2021.02+ds-0apertis0) unstable; urgency=low

  [ Marius Vlad ]
  * Initial release. (Closes: #695849)
    Includes 2 patches from upstream to fix two issues, one related to wayland
    and one about creating two EGLDisplays which might cause issues on some
    platforms.

  [ Andrej Shadura ]
  * Import into Apertis, set component to development.

 -- Andrej Shadura <andrew.shadura@collabora.co.uk>  Thu, 06 May 2021 16:19:26 +0200
